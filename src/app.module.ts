import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './users/users.module';

@Module({
  imports: [MongooseModule.forRoot('mongodb://localhost:27017/test'), UsersModule]
  // imports: [UsersModule],
  // controllers: [AppController],
  // providers: [AppService],
})
export class AppModule { }
