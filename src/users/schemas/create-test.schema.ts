import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import * as mongoose from "mongoose";
import { UserAnswer } from "./../dto/create-test.dto";

@Schema()
class testInfo {
	@Prop({ required: true })
	title: string;
	@Prop({ required: true })
	score: number;
	@Prop({ required: true })
	description: string;
	@Prop({ required: true })
	time: string;
	@Prop({ required: true })
	dateOfCreation: string;
}

Schema()
class OptionContent {
	@Prop({ UserAnswer, required: true })
	answer: UserAnswer;
	@Prop({ required: true })
	description: string;
	@Prop({ required: true })
	id: string;
	@Prop({ required: true })
	key: string;
	@Prop({ required: true })
	option: string
}

@Schema()
class Options {
	@Prop({ required: true })
	option: OptionContent[];
}

@Schema()
class Page {
	@Prop({ required: true })
	question: string;
	@Prop({ required: true })
	options: Options[];
}

@Schema()
export class Test {
	@Prop({ required: true })
	info: testInfo

	@Prop({ required: true })
	McqStore: Page[]
}

@Schema()
export class TestContainer {
	@Prop()
	name: Test
}

export const TestContainerSchema = SchemaFactory.createForClass(TestContainer);

export type userDocument = TestContainer & mongoose.Document;
