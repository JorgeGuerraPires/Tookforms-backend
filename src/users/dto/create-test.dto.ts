import { Type } from "class-transformer";
import { IsString, IsNotEmpty, ValidateNested, IsNumber, IsEnum } from "class-validator";

export enum UserAnswer {
	correct,
	wrong
}

class Info {
	@IsNotEmpty()
	@IsString()
	readonly title: string

	@IsNotEmpty()
	@IsNumber()
	readonly score: number

	@IsNotEmpty()
	@IsString()
	readonly description: string

	@IsNotEmpty()
	@IsString()
	readonly dateOfCreation: string
}

class OptionItem {
	@IsNotEmpty()
	@IsEnum(UserAnswer)
	readonly answer: UserAnswer

	@IsNotEmpty()
	@IsString()
	readonly description: string

	@IsNotEmpty()
	@IsString()
	readonly id: string

	@IsNotEmpty()
	@IsString()
	readonly key: string

	@IsNotEmpty()
	@IsString()
	readonly option: string
}

class Option {
	@ValidateNested({ each: true })
	@IsNotEmpty()
	@Type(() => OptionItem)
	readonly option: OptionItem[]
}

class Options {
	@ValidateNested({ each: true })
	@IsNotEmpty()
	@Type(() => Option)
	readonly options: Option[]

	@IsNotEmpty()
	@IsString()
	readonly question: string
}

// class Page {
// 	@ValidateNested({ each: true })
// 	@IsNotEmpty()
// 	@Type(() => Options)
// 	readonly [key: string]: Options
// }

export class TestDto {
	@ValidateNested({ each: true })
	@IsNotEmpty()
	@Type(() => Info)
	readonly info: Info

	@ValidateNested({ each: true })
	@IsNotEmpty()
	@Type(() => Options)
	readonly McqStore: Options[]
}

export class CreateTestDto {
	@ValidateNested({ each: true })
	@IsNotEmpty()
	@Type(() => TestDto)
	readonly name: TestDto
}