import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CreateTestDto } from './dto/create-test.dto';
import { TestContainer, userDocument } from './schemas/create-test.schema';
import { InjectModel } from '@nestjs/mongoose'
// import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UsersService {

  constructor(
    @InjectModel(TestContainer.name) private readonly userModel: Model<userDocument>,
  ) { }

  // getResults() {
  // }

  // saveCandidateAnswer(saveCandidateAnswerDto: saveCandidateAnswerDto) {
  // }

  async createTest(createTestDto: CreateTestDto): Promise<CreateTestDto> {
    const createdUser = new this.userModel(createTestDto);
    return createdUser.save()
  }

  // createTestAnswer(createTestAnswerDto: createTestAnswerDto) {
  // }

  // updateTestAnswer(updateTestDto: updateTestDto) {
  // }

  // updateTestCount(updateTestCountDto: updateTestCountDto) {
  // }

  // updateTestDataDto(updateTestDataDto: UpdateTestDataDto) {


  // create(createUserDto: CreateUserDto) {
  //   return 'This action adds a new user';
  // }

  // findAll() {
  //   return `This action returns all users`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} user`;
  // }

  // update(id: number, updateUserDto: UpdateUserDto) {
  //   return `This action updates a #${id} user`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} user`;
  // }
}
